package gameFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Timer;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import javax.swing.*;
import javax.swing.text.View;

import org.omg.CORBA.PUBLIC_MEMBER;

import dao.Player;
import dao.PlayerDaoJdbcLmpl;
import music.Music;
public class Main {
	public  static Player play;
	public static void map1() {
		NineGrid aGrid = new NineGrid();
		aGrid.take();
	}
	
	  public static void map2() {
		  NineGrid1 aGrid1=new NineGrid1();
		  aGrid1.map();
	  }
	  
	  public static void map3() {
		  NineGrid2 aGrid2=new NineGrid2();
		  aGrid2.make();
	  }
	  
	public static void choice() {
		JFrame f1 = new JFrame("数字华容道");
		JPanel imaJPanel1;
		ImageIcon backIcon;
		backIcon = new ImageIcon("C:\\Users\\asus\\Desktop\\Java大作业\\图片21.png");
		JLabel label1 = new JLabel(backIcon);
		label1.setBounds(0, 0, backIcon.getIconWidth(), backIcon.getIconHeight());
		imaJPanel1 = (JPanel) f1.getContentPane();
		imaJPanel1.setOpaque(false);
		imaJPanel1.setLayout(new FlowLayout());
		Font t2 = new Font("楷体", Font.BOLD, 100);
		Font t3 = new Font("楷体", Font.BOLD, 50);
		JLabel label4 = new JLabel("难度选择");
		label4.setFont(t2);
		label4.setForeground(Color.pink);
		imaJPanel1.add(label4);
		JButton bt = new JButton("入门");
		bt.setBackground(Color.green);
		bt.setFont(t3);
		bt.setPreferredSize(new Dimension(400, 80));
		bt.setForeground(Color.blue);
		imaJPanel1.add(bt);
		bt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				f1.dispose();
				map1();
			}
		});
		JButton bt1 = new JButton("一般");
		bt1.setBackground(Color.green);
		bt1.setFont(t3);
		bt1.setPreferredSize(new Dimension(400, 80));
		bt1.setForeground(Color.blue);
		imaJPanel1.add(bt1);
		bt1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				f1.dispose();
				map2();
			}
		});
		JButton bt2 = new JButton("困难");
		bt2.setBackground(Color.green);
		bt2.setFont(t3);
		bt2.setPreferredSize(new Dimension(400, 80));
		bt2.setForeground(Color.blue);
		imaJPanel1.add(bt2);
		bt2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				f1.dispose();
				map3();
			}
		});
		JButton bt5=new JButton("返回菜单");
		bt5.setBackground(Color.green);
		bt5.setFont(t3);
		bt5.setPreferredSize(new Dimension(400, 80));
		bt5.setForeground(Color.blue);
		imaJPanel1.add(bt5);
		bt5.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				f1.dispose();
				view();
			}
		});
		f1.getLayeredPane().setLayout(null);
		f1.getLayeredPane().add(label1, new Integer(Integer.MIN_VALUE));
		f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f1.setSize(backIcon.getIconWidth(), backIcon.getIconHeight());
		f1.setLocation(700, 30);
		f1.setVisible(false);
		f1.setVisible(true);
	}

	
	public static void view() {
		JFrame frame = new JFrame("数字华容道");
		JPanel imageJPanel;
		ImageIcon background;
		background = new ImageIcon("C:\\\\Users\\\\asus\\\\Desktop\\\\Java大作业\\\\图片21.png");
		JLabel label = new JLabel(background);
		label.setBounds(0, 0, background.getIconWidth(), background.getIconHeight());
		imageJPanel = (JPanel) frame.getContentPane();
		imageJPanel.setOpaque(false);
		imageJPanel.setLayout(new FlowLayout());
		Font t = new Font("楷体", Font.BOLD, 50);
		Font t1 = new Font("楷体", Font.BOLD, 100);
		JLabel label2 = new JLabel("主菜单");
		label2.setFont(t1);
		label2.setForeground(Color.red);
		imageJPanel.add(label2);
		JButton btn1 = new JButton("开始游戏");
		btn1.setBackground(Color.green);
		btn1.setFont(t);
		btn1.setPreferredSize(new Dimension(400, 80));
		btn1.setForeground(Color.blue);
		imageJPanel.add(btn1);
		btn1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.dispose();
				choice();
			}
		});
		JButton btn2 = new JButton("排行榜");
		btn2.setBackground(Color.green);
		btn2.setFont(t);
		btn2.setPreferredSize(new Dimension(400, 80));
		btn2.setForeground(Color.blue);
		imageJPanel.add(btn2);
		btn2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.dispose();
				Rank.rank();
				
			}
		});
		JButton btn5 = new JButton("游戏规则");
		btn5.setBackground(Color.green);
		btn5.setFont(t);
		btn5.setPreferredSize(new Dimension(400, 80));
		btn5.setForeground(Color.blue);
		imageJPanel.add(btn5);
		btn5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.dispose();
				JFrame f = new JFrame();
				JPanel imageJPanel;
				ImageIcon background;
				background = new ImageIcon("C:\\\\Users\\\\asus\\\\Desktop\\\\Java大作业\\\\图片21.png");
				JLabel label = new JLabel(background);
				label.setBounds(0, 0, background.getIconWidth(), background.getIconHeight());
				imageJPanel = (JPanel) f.getContentPane();
				imageJPanel.setOpaque(false);
				imageJPanel.setLayout(new FlowLayout());
				JLabel jLabel = new JLabel("游戏规则：用尽量少的步数，尽量短的时间，");
				Font t1 = new Font("楷体", Font.BOLD, 20);
				jLabel.setFont(t1);
				jLabel.setForeground(Color.black);
				f.add(jLabel);
				JLabel jLabel1 = new JLabel("将棋盘上的数字方块，按照从左到右、从上到");
				jLabel1.setFont(t1);
				jLabel1.setForeground(Color.black);
				f.add(jLabel1);
				JLabel jLabel2 = new JLabel("下的顺序重新排列整齐。（注意，您只能每次");
				jLabel2.setFont(t1);
				jLabel2.setForeground(Color.black);
				f.add(jLabel2);
				JLabel jLabel3 = new JLabel("移动一次，游戏将根据您的用时以及所用步数");
				jLabel3.setFont(t1);
				jLabel3.setForeground(Color.black);
				f.add(jLabel3);
				JLabel jLabel4 = new JLabel("来决定您是否有资格登上排行榜）。");
				jLabel4.setFont(t1);
				jLabel4.setForeground(Color.black);
				f.add(jLabel4);
				JButton button = new JButton("返回菜单");
				button.setBackground(Color.green);
				button.setFont(t);
				button.setPreferredSize(new Dimension(400, 50));
				button.setForeground(Color.blue);
				imageJPanel.add(button);
				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						f.dispose();
						view();
					}
				});
				f.getLayeredPane().setLayout(null);
				f.getLayeredPane().add(label, new Integer(Integer.MIN_VALUE));
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setSize(background.getIconWidth(), background.getIconHeight());
				f.setResizable(false);
				f.setVisible(true);
				f.setLocation(700, 30);
			}
		});
		JButton btn3 = new JButton("音乐控制");
		btn3.setBackground(Color.green);
		btn3.setFont(t);
		btn3.setPreferredSize(new Dimension(400, 80));
		btn3.setForeground(Color.blue);
		imageJPanel.add(btn3);
		btn3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Music aauMusic = new Music();
				aauMusic.run();

			}
		});
		JButton btn51=new JButton("切换登录");
		btn51.setBackground(Color.green);
		btn51.setFont(t);
		btn51.setPreferredSize(new Dimension(400, 80));
		btn51.setForeground(Color.blue);
		imageJPanel.add(btn51);
		btn51.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.dispose();
				Login.createAndShowGUI();
			}
		});
		JButton btn4 = new JButton("退出游戏");
		btn4.setBackground(Color.green);
		btn4.setFont(t);
		btn4.setPreferredSize(new Dimension(400, 80));
		btn4.setForeground(Color.blue);
		imageJPanel.add(btn4);
		btn4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JOptionPane.showMessageDialog(null, "欢迎下次使用！");
				System.exit(0);
			}
		});
		frame.getLayeredPane().setLayout(null);
		frame.getLayeredPane().add(label, new Integer(Integer.MIN_VALUE));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(background.getIconWidth(), background.getIconHeight());
		frame.setLocation(700, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Music.musicplay();
				Login.createAndShowGUI();
			}
		});
	}
}


