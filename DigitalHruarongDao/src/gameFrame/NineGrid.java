package gameFrame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import dao.Player;
import dao.PlayerDaoJdbcLmpl;

public class NineGrid extends javax.swing.JFrame implements ActionListener {

	private JButton[][] Buttons = new JButton[3][3];
	private int[] arr = new int[9];
	private int count = 0;
	private static String time1;

	/** Creates new form NineGrid */
	public NineGrid() {
		initComponents();
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension d = tk.getScreenSize();
		this.setSize(400, 400);
		this.setTitle("数字华容道");
		this.setLocation((int) (d.getWidth() - 400) / 2, (int) (d.getHeight() - 400) / 2);

		for (int i = 0; i < 8; i++) {
			this.arr[i] = i + 1;
		}
		this.arr[8] = -1;

		this.arr[5] = -1;
		this.arr[8] = 6;

		for (int i = 0; i < 8; i++) {
			int idx = (int) (Math.random() * 8);
			int tmp = this.arr[7];
			this.arr[7] = this.arr[idx];
			this.arr[idx] = tmp;
		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (this.arr[i * 3 + j] != -1) {
					this.Buttons[i][j] = new JButton("" + this.arr[i * 3 + j]);
					this.Buttons[i][j].addActionListener(this);
					Font tFont=new Font("楷体",Font.BOLD,20);
					this.Buttons[i][j].setForeground(Color.black);
					this.Buttons[i][j].setFont(tFont);
					this.getContentPane().add(this.Buttons[i][j]);
				} else {
					this.Buttons[i][j] = new JButton("");
					this.Buttons[i][j].addActionListener(this);
					Font tFont=new Font("楷体",Font.BOLD,20);
					this.Buttons[i][j].setForeground(Color.black);
					this.Buttons[i][j].setFont(tFont);
					this.getContentPane().add(this.Buttons[i][j]);
					this.Buttons[i][j].setEnabled(false);
				}
			}
		}
		JButton button=new JButton("退出");
		Font t=new Font("楷体",Font.BOLD,30);
		button.setFont(t);		
		button.setBackground(Color.white);
		button.setForeground(Color.red);
		this.add(button);
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				Main.view();
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		getContentPane().setLayout(new java.awt.GridLayout(4,3));

		pack();
	}// </editor-fold>
	public void take() {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new NineGrid().setVisible(true);
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
				time1 = df.format(new Date());
			}
		});
	}
	private boolean isSucceed() {
		boolean flag = true;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (!this.Buttons[i][j].getText().equals(""))
					if (!this.Buttons[i][j].getText().equals("" + (i * 3 + j + 1))) {
						return false;
					}
			}
		}
		return true;
	}
	
	public void showmessage() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		String time2=df.format(new Date());
		JFrame frame = new JFrame("恭喜完成！");
		JPanel jPanel = new JPanel();
		JLabel label = new JLabel("您所用的步数是:    " + count);
		JLabel label2 = new JLabel("您所用的时间是:    "+time1+"-"+time2);
		JButton button = new JButton("确定");
		frame.setSize(1059,451);
		Font t1 = new Font("宋体", Font.BOLD, 50);
		button.setBackground(Color.green);
		button.setFont(t1);
		button.setPreferredSize(new Dimension(400, 80));
		button.setForeground(Color.blue);
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.dispose();
				JOptionPane.showMessageDialog(null, "即将返回主菜单");
				Main.view();
			}
		});
		Font t = new Font("楷体", Font.BOLD, 30);
		label.setFont(t);
		label2.setFont(t);
		jPanel.add(label);
		jPanel.add(label2);
		jPanel.add(button);
		jPanel.setLayout(new FlowLayout());
		frame.add(jPanel);
		frame.setVisible(true);
		frame.setLocation(800, 300);

	}

	public void actionPerformed(ActionEvent e) {
		int i = 0, j = 0;
		boolean in = false;
		for (i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				if (e.getSource() == this.Buttons[i][j]) {
					in = true;
					count++;
					System.out.println(count);
					break;
				}
			}
			if (in)
				break;
		}
		

		if ((i >= 0 && (j - 1) >= 0) && (!this.Buttons[i][j - 1].isEnabled())) {
			String tmp = this.Buttons[i][j].getText();
			this.Buttons[i][j].setText(this.Buttons[i][j - 1].getText());
			this.Buttons[i][j - 1].setText(tmp);
			this.Buttons[i][j - 1].setEnabled(true);
			this.Buttons[i][j].setEnabled(false);
			int x=1;
			if (this.isSucceed()) {
				JOptionPane.showConfirmDialog(null, "You Win!!!!");
				PlayerDaoJdbcLmpl judgethree=new PlayerDaoJdbcLmpl();
				judgethree.insertRank(Main.play, x, count);
				this.dispose();
				showmessage();
			}
			return;
		}
		if ((i >= 0 && (j + 1) < 3) && (!this.Buttons[i][j + 1].isEnabled())) {
			String tmp = this.Buttons[i][j].getText();
			this.Buttons[i][j].setText(this.Buttons[i][j + 1].getText());
			this.Buttons[i][j + 1].setText(tmp);
			this.Buttons[i][j + 1].setEnabled(true);
			this.Buttons[i][j].setEnabled(false);
			int x=1;
			if (this.isSucceed()) {
				JOptionPane.showConfirmDialog(null, "You Win!!!!");
				PlayerDaoJdbcLmpl judgethree=new PlayerDaoJdbcLmpl();
				judgethree.insertRank(Main.play, x, count);
				this.dispose();
				showmessage();
			}
			return;
		}
		if ((i - 1 >= 0 && j >= 0) && (!this.Buttons[i - 1][j].isEnabled())) {
			String tmp = this.Buttons[i][j].getText();
			this.Buttons[i][j].setText(this.Buttons[i - 1][j].getText());
			this.Buttons[i - 1][j].setText(tmp);
			this.Buttons[i - 1][j].setEnabled(true);
			this.Buttons[i][j].setEnabled(false);
			int x=1;
			if (this.isSucceed()) {
				JOptionPane.showConfirmDialog(null, "You Win!!!!");
				PlayerDaoJdbcLmpl judgethree=new PlayerDaoJdbcLmpl();
				judgethree.insertRank(Main.play,x, count);
				this.dispose();
				showmessage();
			}
			return;
		}
		if ((i + 1 < 3 && j >= 0) && (!this.Buttons[i + 1][j].isEnabled())) {
			String tmp = this.Buttons[i][j].getText();
			this.Buttons[i][j].setText(this.Buttons[i + 1][j].getText());
			this.Buttons[i + 1][j].setText(tmp);
			this.Buttons[i + 1][j].setEnabled(true);
			this.Buttons[i][j].setEnabled(false);
			int x=1;
			if (this.isSucceed()) {
				JOptionPane.showConfirmDialog(null, "You Win!!!!");
				PlayerDaoJdbcLmpl judgethree=new PlayerDaoJdbcLmpl();
				judgethree.insertRank(Main.play, x, count);
				this.dispose();
				showmessage();
			}
			return;
		}

	}
}
