package dao;
/**
* DAO模式中的DAO接口
* 
* @author timmy
*
*/
import java.util.ArrayList;
import java.util.List;

public interface PlayerDao {
	/**
	 * 注册功能，通过判断数据库中是否存在输入的用户信息
	 * @param play
	 * @return
	 */
	public boolean register(Player play);
	/**
	 * 登录功能，通过获取数据库里面的用户信息和输入账户密码进行判断，判断登录是否成功
	 * @param play
	 * @return
	 */
	public boolean login(Player play);
	/**
	 * 将数据库中的用户信息以ArryList的形式返回
	 * @return
	 */
	public ArrayList<Player> getAllPlayers();
	/**
	 * 若游戏结果比记录中优秀，则改变数据库中的记录，若没有突破记录，则记录不计入数据库
	 * @param play
	 * @param i
	 * @param count
	 * @return
	 */
	public boolean insertRank(Player play,int i,int count);
	/**
	 * 获取记录
	 * @param name
	 * @return
	 */
	public Player getRecord(String name);
	/*
	 * 
	 */

}
