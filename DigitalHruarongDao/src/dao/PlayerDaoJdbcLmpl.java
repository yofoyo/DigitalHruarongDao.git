package dao;
/**
* 	DAO模式的实现类，与使用JBDC工具类（作者：zhrb）连接数据库
* 拥有判断登录时账号密码与数据库中账号信息是否相同的方法，和注册时插入用户信息的方法
* 拥有读取用户信息，更新记录，输出所有用户记录的方法
* @author timmy
*
*/
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import sun.security.util.Password;
import utils.JDBCUtil;
public class PlayerDaoJdbcLmpl implements PlayerDao{
	@Override
	public boolean register(Player play) {
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		PreparedStatement pStatement = null;
		String sql = "select * from AccoutNumber";

		int flag=0;
		try {
			conn = JDBCUtil.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while(rs.next()){
				String name=rs.getString("name");
				if(name.equals(play.getName())) {
					flag=1;
					break;
				}
			}
			if(flag==0) {
				String strSql = "insert into AccoutNumber(name,passWord,recordOne,recordTwo,recordThree) values(?,?,0,0,0)";
				pStatement = conn.prepareStatement(strSql);
				pStatement.setString(1,play.getName());
				pStatement.setString(2,play.getPassWord());
				pStatement.executeUpdate();
				return true;
			}
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			JDBCUtil.realeaseAll(rs,stat, conn);
		}
		return false;
	}

	@Override
	public boolean login(Player play) {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from AccoutNumber";
		try {
			conn = JDBCUtil.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while(rs.next()){
				String name=rs.getString("name");
				String passWord=rs.getString("passWord");
				if(name.equals(play.getName())&&passWord.equals(play.getPassWord())) {
					return true;
				}
			}
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			JDBCUtil.realeaseAll(rs,stat, conn);
		}
		return false;
	}


	
	@Override
	public boolean insertRank(Player play,int i,int count) {
		Connection conn = null;
		PreparedStatement pstat = null;
		PreparedStatement pstat1=null;
		ResultSet rs = null;
		String sql = "select * from accoutnumber where name like ?";
		int n1=1;
		int n2=2;
		int n3=3;
		int flag=0;
		try {
			conn = JDBCUtil.getConnection();
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, play.getName());
			String sql1="update accoutnumber set recordOne=? where name=?";
			pstat1=conn.prepareStatement(sql1);
			rs = pstat.executeQuery();
			
			if(i==n1)
			{
			while(rs.next()){
				int record = rs.getInt("recordOne");
				if(record>play.getRecordOne()&&record!=0)
				{
					break;
				}else {
					pstat1=conn.prepareStatement(sql1);
					pstat1.setInt(1, count);
					pstat1.setString(2,play.getName());
					pstat1.execute();
					flag=1;
			}
			}
			}else if(i==n2) {
				while(rs.next()){
					int record = rs.getInt("recordTwo");
					if(record>play.getRecordOne()&&record!=0)
					{
						break;
					}else {
						pstat1=conn.prepareStatement(sql1);
						pstat1.setInt(1, count);
						pstat1.setString(2,play.getName());
						pstat1.execute();
						flag=1;
				}
				}
			}else if(i==n3) {
				while(rs.next()){
					int record = rs.getInt("recordThree");
					if(record>play.getRecordOne()&&record!=0)
					{
						break;
					}else {
						pstat1=conn.prepareStatement(sql1);
						pstat1.setInt(1, count);
						pstat1.setString(2,play.getName());
						pstat1.execute();
						flag=1;
				}
				}
			}
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			JDBCUtil.realeaseAll(rs,pstat, conn);
		}
		if(flag==1) {
			System.out.println("突破界限！");
			return true;
		}else {
			System.out.println("再接再厉！");
			return false;
		}
	}

	@Override
	public ArrayList<Player> getAllPlayers() {
		// TODO Auto-generated method stub
		ArrayList <Player> p=new ArrayList<Player>();
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from AccoutNumber";
		try {
			conn = JDBCUtil.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while(rs.next()){
				Player play=new Player();
				play.setName(rs.getString(1));
				play.setPassWord(rs.getString(2));
				play.setRecordOne(rs.getInt(3));
				play.setRecordTwo(rs.getInt(4));
				play.setRecordThree(rs.getInt(5));
				p.add(play);
				}
			return p;
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			JDBCUtil.realeaseAll(rs,stat, conn);
		}
		return null;
	}

	@Override
	public Player getRecord(String name) {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement pstat = null;
		ResultSet rs = null;
		String sql = "select * from accoutnumber where name like ?";
		int flag=0;
		Player player=new Player();
		try {
			conn = JDBCUtil.getConnection();
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, name);
			rs = pstat.executeQuery();
			while(rs.next()){
				player.setName(rs.getString("name"));
				player.setPassWord(rs.getString("Password"));
				player.setRecordOne(rs.getInt("recordOne"));
				player.setRecordTwo(rs.getInt("recordTwo"));
				player.setRecordThree(rs.getInt("recordThree"));}
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			JDBCUtil.realeaseAll(rs,pstat, conn);
		}
		return player;
	}
}

	

	
		
	

