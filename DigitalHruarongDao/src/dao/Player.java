package dao;
/**
 * 用户类，即dao模式操作的实体类，进行用户账号信息以及所创造的记录的存储
* 
* @author timmy
*
*/
public class Player {
	private String name;
	private String passWord;
	private int recordOne;
	private int recordTwo;
	private int recordThree;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public int getRecordOne() {
		return recordOne;
	}
	public void setRecordOne(int recordOne) {
		this.recordOne = recordOne;
	}
	public int getRecordTwo() {
		return recordTwo;
	}
	public void setRecordTwo(int recordTwo) {
		this.recordTwo = recordTwo;
	}
	public int getRecordThree() {
		return recordThree;
	}
	public void setRecordThree(int recordThree) {
		this.recordThree = recordThree;
	}
	@Override
	public String toString() {
		return "Player [name=" + name +  ", recordOne=" + recordOne + ", recordTwo="
				+ recordTwo + ", recordThree=" + recordThree + "]";
	}

}
